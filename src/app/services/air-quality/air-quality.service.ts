import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { CityListResponse } from 'src/models/api-response/city-list-response.model';
import { environment } from 'src/environments/environment';
import { City } from 'src/models/city.model';
import { MeasurementsResponse } from 'src/models/api-response/measurements-response.model';
import { Latest } from 'src/models/latest.model';

@Injectable({
  providedIn: 'root'
})
export class AirQualityService {
  private countryCode = 'GB';
  private pageLimit = 200;
  private parameters = ['pm25', 'so2', 'o3', 'no2'];

  constructor(private http: HttpClient) { }

  getCityList(): Observable<Array<City>> {
    return this.http.get<CityListResponse>
      (`${environment.openAqUrl}cities?country=${this.countryCode}&limit=${this.pageLimit}&order_by=city`)
      .pipe(map(response => response.results));
  }

  selectCity(city: string): Observable<Array<Latest>> {
    const params = this.createRequestParameters();
    return this.http.get<MeasurementsResponse>(`${environment.openAqUrl}latest?city=${city}${params}`)
      .pipe(map(response => response.results));
  }

  private createRequestParameters(): string {
    const response = '';
    this.parameters.forEach(param => response.concat(`&parameter[]=${param}`));
    return response;
  }
}

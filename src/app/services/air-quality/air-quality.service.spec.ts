import { TestBed } from '@angular/core/testing';

import { AirQualityService } from './air-quality.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

describe('AirQualityService', () => {
  let httpMock: HttpTestingController;
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        AirQualityService
      ],
      imports: [
        HttpClientTestingModule
      ]
    });
    httpMock = TestBed.get(HttpTestingController);
  });

  it('should be created', () => {
    const service: AirQualityService = TestBed.get(AirQualityService);
    expect(service).toBeTruthy();
  });
});

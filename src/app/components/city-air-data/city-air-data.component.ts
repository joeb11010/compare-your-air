import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { CardData } from 'src/models/card-data.model';
import { PeriodType } from 'src/models/period-type.model';

@Component({
  selector: 'app-city-air-data',
  templateUrl: './city-air-data.component.html',
  styleUrls: ['./city-air-data.component.scss']
})
export class CityAirDataComponent implements OnInit {
  lastUpdated: string;
  @Input() data: CardData;
  @Output() closeCard: EventEmitter<void> = new EventEmitter<void>();
  constructor() { }

  ngOnInit() {
    this.lastUpdated = this.createUpdatedString(this.data.updated, new Date());
  }

  createUpdatedString(dateFrom: Date, dateTo: Date): string {
    const diffMins = Math.abs( (dateTo.getTime() - dateFrom.getTime()) / 60000);
    const updated = this.getPeriodType(diffMins);
    const plural = updated.diff > 1 ? 'S' : '';
    return `${updated.diff} ${updated.period}${plural}`;
  }

  private getPeriodType(minuteDiff: number): PeriodType {
    if (minuteDiff < 60) {
      return new PeriodType(Math.ceil(minuteDiff), 'MINUTE');
    }
    const diffHours = minuteDiff / 60;
    if (diffHours < 24) {
      return new PeriodType(Math.ceil(diffHours), 'HOUR');
    }
    const diffDays = diffHours / 24;
    if (diffDays < 14) {
      return new PeriodType(Math.ceil(diffDays), 'DAY');
    }
    const diffWeeks = diffDays / 7;
    if (diffWeeks < 8) {
      return new PeriodType(Math.ceil(diffWeeks), 'WEEK');
    }
    const diffMonths = diffWeeks / 4;
    return new PeriodType(Math.ceil(diffMonths), 'MONTH');
  }

  close(): void {
    this.closeCard.emit();
  }
}

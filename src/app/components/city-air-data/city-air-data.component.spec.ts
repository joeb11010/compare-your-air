import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CityAirDataComponent } from './city-air-data.component';
import { CardData } from 'src/models/card-data.model';
import { FieldValue } from 'src/models/field-values.model';
import { Component, ViewChild } from '@angular/core';
import { MatCardModule, MatIconModule } from '@angular/material';

describe('CityAirDataComponent', () => {
  let component: TestHostComponent;
  let fixture: ComponentFixture<TestHostComponent>;
  const dateNow = '2019-10-10';
  beforeEach(async(() => {
    const UnmockedDate = Date;

    TestBed.configureTestingModule({
      declarations: [CityAirDataComponent, TestHostComponent],
      imports: [
        MatCardModule,
        MatIconModule
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestHostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should return string: "1 MINUTE" if date param is 1 minute ago or less', () => {
    const dateTo = new Date(dateNow);
    const dateFrom = new Date(new Date(dateNow).setMinutes(new Date(dateNow).getMinutes() - 1 ));
    expect(component.cityAirDataComponent.createUpdatedString(dateFrom, dateTo)).toBe('1 MINUTE');
  });

  it('should return string "5 MINUTES" if date param is 5 minutes ago ', () => {
    const dateTo = new Date(dateNow);
    const dateFrom = new Date(new Date(dateNow).setMinutes(new Date(dateNow).getMinutes() - 5 ));
    expect(component.cityAirDataComponent.createUpdatedString(dateFrom, dateTo)).toBe('5 MINUTES');
  });

  it('should return string "1 HOUR" if date param is 1 hour ago', () => {
    const dateTo = new Date(dateNow);
    const dateFrom = new Date(new Date(dateNow).setHours(new Date(dateNow).getHours() - 1 ));
    expect(component.cityAirDataComponent.createUpdatedString(dateFrom, dateTo)).toBe('1 HOUR');
  });

  it('should return string "5 HOURS" if date param is 5 hours ago', () => {
    const dateTo = new Date(dateNow);
    const dateFrom = new Date(new Date(dateNow).setHours(new Date(dateNow).getHours() - 5 ));
    expect(component.cityAirDataComponent.createUpdatedString(dateFrom, dateTo)).toBe('5 HOURS');
  });

  it('should return string "1 DAY" if date param is 1 day ago', () => {
    const dateTo = new Date(dateNow);
    const dateFrom = new Date(new Date(dateNow).setHours(new Date(dateNow).getHours() - 24 ));
    expect(component.cityAirDataComponent.createUpdatedString(dateFrom, dateTo)).toBe('1 DAY');
  });

  it('should return string "7 DAYS" ago if date param is 21 days ago', () => {
    const dateTo = new Date(dateNow);
    const dateFrom = new Date(new Date(dateNow).setHours(new Date(dateNow).getHours() - (24 * 7)));
    expect(component.cityAirDataComponent.createUpdatedString(dateFrom, dateTo)).toBe('7 DAYS');
  });

  it('should return string "2 WEEKS" if date param is 2 weeks ago', () => {
    const dateTo = new Date(dateNow);
    const dateFrom = new Date(new Date(dateNow).setHours(new Date(dateNow).getHours() - (24 * 14) ));
    expect(component.cityAirDataComponent.createUpdatedString(dateFrom, dateTo)).toBe('2 WEEKS');
  });

  it('should return string "2 MONTHS" if date param is 2 months ago', () => {
    const dateTo = new Date(dateNow);
    const dateFrom = new Date(new Date(dateNow).setHours(new Date(dateNow).getHours() - (24 * 7 * 8) ));
    expect(component.cityAirDataComponent.createUpdatedString(dateFrom, dateTo)).toBe('2 MONTHS');
  });

  @Component({
    selector: `test-host-component`,
    template: `<app-city-air-data [data]="input"></app-city-air-data>`
  })
  class TestHostComponent {
    @ViewChild(CityAirDataComponent, { read: false, static: true })
    public cityAirDataComponent: CityAirDataComponent;
    input = new CardData('location', 'city', 'country', [new FieldValue('field', 1)], new Date('2019-01-01'));
  }
});

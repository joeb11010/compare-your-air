import { Component, Output, Input, OnInit } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { MatOptionSelectionChange } from '@angular/material';
import { FormControl } from '@angular/forms';
import { startWith, map, debounce, debounceTime } from 'rxjs/operators';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-city-list',
  templateUrl: './city-list.component.html',
  styleUrls: ['./city-list.component.scss']
})
export class CityListComponent implements OnInit {
  filteredItems: Observable<Array<string>>;
  myControl = new FormControl();

  @Input() list: Array<string>;
  @Output() selectedCity: EventEmitter<string> = new EventEmitter<string>();
  constructor() { }

  ngOnInit() {
    this.filteredItems = this.myControl.valueChanges
    .pipe(
      debounceTime(300),
      startWith(''),
      map(value => this.filter(value))
    );
  }

  filter(value: string): Array<string> {
    const filterValue = value.toLowerCase();
    return this.list.filter(item => item.toLowerCase().includes(filterValue));
  }

  select(event: MatOptionSelectionChange): void {
    if (event.isUserInput) {
      this.selectedCity.emit(event.source.value);
    }
    setTimeout(() => this.myControl.setValue(''), 10);
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CityListComponent } from './city-list.component';
import { Component, ViewChild } from '@angular/core';
import {
  MatFormFieldModule,
  MatOptionModule,
  MatInputModule,
  MatAutocompleteModule,
  MatOptionSelectionChange,
  MatOption,
  MatIconModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Mock } from 'ts-mocks';

describe('CityListComponent', () => {
  let component: TestHostComponent;
  let fixture: ComponentFixture<TestHostComponent>;

  let mockMatOption: Mock<MatOption>;
  let mockChange: Mock<MatOptionSelectionChange>;

  beforeEach(async(() => {
    mockMatOption = new Mock<MatOption>({value: 'test'});
    mockChange = new Mock<MatOptionSelectionChange>({source: mockMatOption.Object});
    TestBed.configureTestingModule({
      declarations: [ CityListComponent, TestHostComponent ],
      imports: [
        MatFormFieldModule,
        MatOptionModule,
        MatInputModule,
        MatAutocompleteModule,
        FormsModule,
        BrowserAnimationsModule,
        MatIconModule,
        ReactiveFormsModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestHostComponent);
    component = fixture.componentInstance;
    component.resetSelection();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should filter list based on input value changing', () => {
      expect(component.cityListComponent.filter('2')).toEqual(['Option2']);
  });

  it('should emit value of change event on select when user input is true', () => {
    mockChange.extend({isUserInput: true});
    component.cityListComponent.select(mockChange.Object);
    expect(component.output).toEqual(mockChange.Object.source.value);
  });

  it('should not emit value of change event on select when user input is false', () => {
    mockChange.extend({isUserInput: false});
    component.cityListComponent.select(mockChange.Object);
    expect(component.output).toEqual('');
  });

  @Component({
    selector: `test-host-component`,
    template: `<app-city-list [list]="input" (selectedCity)="setSelection($event)"></app-city-list>`
  })
  class TestHostComponent {
    @ViewChild(CityListComponent, { read: false, static: true })
    public cityListComponent: CityListComponent;
    input = ['Option1', 'Option2', 'Option3', 'Option4'];
    output = '';

    resetSelection() {
      this.output = '';
    }

    setSelection(city: string) {
      this.output = city;
    }
  }
});

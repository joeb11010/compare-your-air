import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { CityListComponent } from './components/city-list/city-list.component';
import { MockComponent } from 'ng-mocks';
import { CityAirDataComponent } from './components/city-air-data/city-air-data.component';
import { Mock } from 'ts-mocks';
import { AirQualityService } from './services/air-quality/air-quality.service';
import { of } from 'rxjs';
import { Latest } from 'src/models/latest.model';
import { City } from 'src/models/city.model';
import { CardData } from 'src/models/card-data.model';

describe('AppComponent', () => {
  let app: AppComponent;
  let mockAirQualityService: Mock<AirQualityService>;
  let mockLatest: Array<Latest>;
  let mockCityList: Array<City>;
  beforeEach(async(() => {
    mockLatest = getMockLatest();
    mockCityList = getMockCityList();
    mockAirQualityService = new Mock<AirQualityService>({ selectCity: () => of(mockLatest) });
    mockAirQualityService.extend({ getCityList: () => of(mockCityList) });
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        MockComponent(CityListComponent),
        MockComponent(CityAirDataComponent),
      ],
      providers: [
        { provide: AirQualityService, useValue: mockAirQualityService.Object }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    const fixture = TestBed.createComponent(AppComponent);
    app = fixture.debugElement.componentInstance;
    fixture.detectChanges();
  });

  it('should create the app', () => {
    expect(app).toBeTruthy();
  });

  it('should create an array of city names on init', () => {
    expect(app.cityOptions).toBeTruthy(mockCityList.map(x => x.city));
  });

  it('should create a card data object from latest data with same location', () => {
    app.selectCity('testCity');
    expect(app.selectedCities[0].location).toEqual(mockLatest[0].location);
  });

  it('should create a card data object from latest data with same city', () => {
    app.selectCity('testCity');
    expect(app.selectedCities[0].city).toEqual(mockLatest[0].city);
  });

  it('should create a card data object from latest data with uppercase params', () => {
    app.selectCity('testCity');
    expect(app.selectedCities[0].measurements[0].field).toEqual(mockLatest[0].measurements[0].parameter.toUpperCase());
  });

  function getMockCityList(): Array<City> {
    return [
      {
        city: 'testCity',
        count: 1,
        country: 'testCountry',
        locations: 2
      },
      {
        city: 'testCity2',
        count: 3,
        country: 'testCountry2',
        locations: 4
      }
    ];
  }

  function getMockLatest(): Array<Latest> {
    return [
      {
        city: 'testCity',
        country: 'testCountry',
        location: 'testLocation',
        coordinates: {
          longitude: 10,
          latitude: 20
        },
        distance: 30,
        measurements: [{
          averagingPeriod: {
            unit: 'testUnit',
            value: 40,
          },
          lastUpdated: '2019-02-02',
          parameter: 'testParam',
          sourceName: 'source',
          unit: 'testUnit2',
          value: 50
        }]
      }
    ];
  }
});

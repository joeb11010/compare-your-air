import { Component, OnInit } from '@angular/core';
import { AirQualityService } from './services/air-quality/air-quality.service';
import { Latest } from 'src/models/latest.model';
import { CardData } from 'src/models/card-data.model';
import { Measurement } from 'src/models/measurement.model';
import { FieldValue } from 'src/models/field-values.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  selectedCities: Array<CardData> = [];
  cityOptions: Array<string>;
  error: boolean;
  private country = 'United Kingdom';
  constructor(private airQualityService: AirQualityService) { }

  ngOnInit(): void {
    this.airQualityService.getCityList().subscribe(cities => {
      this.cityOptions = cities.map(x => x.city);
    }, () => this.error = true);
  }

  selectCity(city: string): void {
    this.airQualityService.selectCity(city).subscribe(
      data =>  this.selectedCities.push(this.createCardData(data[0])),
      () => this.error = true);
  }

  closeCard(index: number) {
    this.selectedCities.splice(index, 1);
  }

  private createCardData(data: Latest): CardData {
    const orderedMeasurements = data.measurements.sort((a: Measurement, b: Measurement) => {
      return new Date(b.lastUpdated).getTime() - new Date(a.lastUpdated).getTime();
    });
    return new CardData(data.location, data.city, this.country, this.createMeasurements(data.measurements),
      new Date(orderedMeasurements[0].lastUpdated));
  }

  private createMeasurements(measurements: Array<Measurement>): Array<FieldValue> {
    const response: Array<FieldValue> = [];
    measurements.forEach(m => response.push(new FieldValue(m.parameter.toUpperCase(), m.value)));
    return response;
  }
}

import { Measurement } from './measurement.model';
import { CustomCoordinates } from './coordinates.model';

export class Latest {
    city: string;
    country: string;
    coordinates: CustomCoordinates;
    distance: number;
    location: string;
    measurements: Array<Measurement>;
}

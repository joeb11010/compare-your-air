import { AveragingPeriod } from './averaging-period.model';

export class Measurement {
    averagingPeriod: AveragingPeriod;
    parameter: string;
    sourceName: string;
    value: number;
    unit: string;
    lastUpdated: string;
}

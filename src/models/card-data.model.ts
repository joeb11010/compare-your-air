import { FieldValue } from './field-values.model';
import { Measurement } from './measurement.model';

export class CardData {
    updated: Date;
    location: string;
    city: string;
    country: string;
    measurements: Array<FieldValue>;

    constructor(location: string, city: string, country: string, measurements: Array<FieldValue>, updated: Date) {
        this.location = location;
        this.city = city;
        this.country = country;
        this.measurements = measurements;
        this.updated = updated;
    }

}

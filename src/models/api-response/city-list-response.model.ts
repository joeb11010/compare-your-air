import { Meta } from '@angular/platform-browser';
import { City } from '../city.model';

export class CityListResponse {
    meta: Meta;
    results: Array<City>;
}

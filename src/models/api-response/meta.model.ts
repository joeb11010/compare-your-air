export class Meta {
    found: number;
    license: string;
    limit: number;
    name: string;
    page: string;
    website: string;
}

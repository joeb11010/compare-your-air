import { Meta } from '@angular/platform-browser';
import { Latest } from '../latest.model';

export class MeasurementsResponse {
    meta: Meta;
    results: Array<Latest>;
}

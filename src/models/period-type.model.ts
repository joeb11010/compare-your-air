export class PeriodType {
    diff: number;
    period: string;

    constructor(diff: number, period: string) {
        this.diff = diff;
        this.period = period;
    }
}

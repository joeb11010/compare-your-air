export class CustomCoordinates {
    longitude: number;
    latitude: number;
}

export class FieldValue {
    field: string;
    value: number;

    constructor(field: string, value: number) {
        this.field = field;
        this.value = value;
    }
}
